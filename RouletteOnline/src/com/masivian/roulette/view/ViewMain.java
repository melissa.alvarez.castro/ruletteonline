package com.masivian.roulette.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.masivian.roulette.classes.Bet;
import com.masivian.roulette.classes.Casino;

import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;

public class ViewMain extends JFrame implements ActionListener {

	private JPanel contentPane;
	private Casino casino = new Casino();
	private JLabel lblResultCreateRoulette;
	private JLabel lblResultOpenRoulette;
	private JLabel lblResultBets;
	private JButton btnCreateRoulette;
	private JButton btnOpenRoulette;
	private JButton btnBets;
	private JComboBox comboBoxOpenRoulette;
	private JComboBox comboBoxBetsRoulette;
	private JComboBox comboBoxBetsClients;
	private JComboBox comboBoxColor;
	private JComboBox comboBoxNumber;
	private JTextField txtValueBet;
	private JTabbedPane TabBetTypes;
	private JLayeredPane TabBetNumber;
	private JLayeredPane TabBetColor;
	private JLayeredPane tabCloseBet;
	private JPanel panelCloseBets;
	private JLabel lblCloseBets;
	private JComboBox comboBoxCloseBets;
	private JButton btnCloseBets;
	private JTable tableCloseBets;
	private JTable tableAllRoulettes;

	public ViewMain() {
		
		/**
		 * These lines of code are autogenerated
		 */
		
		setTitle("Roulette online");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(tabbedPane,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(tabbedPane,
				GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE));

		JLayeredPane tabCreateRoulette = new JLayeredPane();
		tabbedPane.addTab("Crear ruleta", null, tabCreateRoulette, null);

		JPanel panelCreateRoulette = new JPanel();
		GroupLayout gl_tabCreateRoulette = new GroupLayout(tabCreateRoulette);
		gl_tabCreateRoulette.setHorizontalGroup(gl_tabCreateRoulette.createParallelGroup(Alignment.LEADING)
				.addComponent(panelCreateRoulette, GroupLayout.DEFAULT_SIZE, 419, Short.MAX_VALUE));
		gl_tabCreateRoulette.setVerticalGroup(gl_tabCreateRoulette.createParallelGroup(Alignment.LEADING)
				.addComponent(panelCreateRoulette, GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE));

		JLabel lblCreateRoulette = new JLabel("Crear nueva ruleta");
		lblCreateRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));

		btnCreateRoulette = new JButton("crear");
		btnCreateRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCreateRoulette.addActionListener(this);

		lblResultCreateRoulette = new JLabel("");
		lblResultCreateRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GroupLayout gl_panelCreateRoulette = new GroupLayout(panelCreateRoulette);
		gl_panelCreateRoulette.setHorizontalGroup(gl_panelCreateRoulette.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCreateRoulette.createSequentialGroup().addContainerGap()
						.addComponent(lblCreateRoulette).addGap(18).addComponent(btnCreateRoulette).addGap(18)
						.addComponent(lblResultCreateRoulette, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
						.addContainerGap()));
		gl_panelCreateRoulette.setVerticalGroup(gl_panelCreateRoulette.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCreateRoulette.createSequentialGroup().addContainerGap()
						.addGroup(gl_panelCreateRoulette.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblCreateRoulette).addComponent(btnCreateRoulette)
								.addComponent(lblResultCreateRoulette))
						.addContainerGap(198, Short.MAX_VALUE)));
		panelCreateRoulette.setLayout(gl_panelCreateRoulette);
		tabCreateRoulette.setLayout(gl_tabCreateRoulette);

		JLayeredPane tabOpenRoulette = new JLayeredPane();
		tabbedPane.addTab("Abrir ruleta", null, tabOpenRoulette, null);

		JPanel panelOpenRoulette = new JPanel();
		GroupLayout gl_tabOpenRoulette = new GroupLayout(tabOpenRoulette);
		gl_tabOpenRoulette.setHorizontalGroup(gl_tabOpenRoulette.createParallelGroup(Alignment.LEADING)
				.addComponent(panelOpenRoulette, GroupLayout.DEFAULT_SIZE, 419, Short.MAX_VALUE));
		gl_tabOpenRoulette.setVerticalGroup(gl_tabOpenRoulette.createParallelGroup(Alignment.LEADING)
				.addComponent(panelOpenRoulette, GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE));

		JLabel lblOpenRoulette = new JLabel("Seleccione la ruleta que desea abrir");
		lblOpenRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));

		comboBoxOpenRoulette = new JComboBox();
		comboBoxOpenRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lblResultOpenRoulette = new JLabel("");
		lblResultOpenRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));

		btnOpenRoulette = new JButton("Abrir");
		btnOpenRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnOpenRoulette.addActionListener(this);

		loadTabOpenRoulette();

		GroupLayout gl_panelOpenRoulette = new GroupLayout(panelOpenRoulette);
		gl_panelOpenRoulette.setHorizontalGroup(gl_panelOpenRoulette.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelOpenRoulette.createSequentialGroup().addContainerGap().addGroup(gl_panelOpenRoulette
						.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelOpenRoulette.createSequentialGroup().addComponent(lblOpenRoulette).addGap(18)
								.addComponent(comboBoxOpenRoulette, GroupLayout.PREFERRED_SIZE, 117,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18).addComponent(btnOpenRoulette).addContainerGap())
						.addGroup(gl_panelOpenRoulette.createSequentialGroup()
								.addComponent(lblResultOpenRoulette, GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
								.addGap(316)))));
		gl_panelOpenRoulette.setVerticalGroup(gl_panelOpenRoulette.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelOpenRoulette.createSequentialGroup().addContainerGap().addGroup(gl_panelOpenRoulette
						.createParallelGroup(Alignment.BASELINE).addComponent(lblOpenRoulette)
						.addComponent(comboBoxOpenRoulette, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(btnOpenRoulette)).addGap(18).addComponent(lblResultOpenRoulette)
						.addContainerGap(171, Short.MAX_VALUE)));
		panelOpenRoulette.setLayout(gl_panelOpenRoulette);
		tabOpenRoulette.setLayout(gl_tabOpenRoulette);

		JLayeredPane tabBets = new JLayeredPane();
		tabbedPane.addTab("Apuestas", null, tabBets, null);

		JPanel panelBets = new JPanel();
		GroupLayout gl_tabBets = new GroupLayout(tabBets);
		gl_tabBets.setHorizontalGroup(gl_tabBets.createParallelGroup(Alignment.LEADING).addComponent(panelBets,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 619, Short.MAX_VALUE));
		gl_tabBets.setVerticalGroup(gl_tabBets.createParallelGroup(Alignment.LEADING).addComponent(panelBets,
				GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE));

		JLabel lblRoulette = new JLabel("Ruleta");
		lblRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));

		comboBoxBetsRoulette = new JComboBox();
		comboBoxBetsRoulette.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JLabel lblCliente = new JLabel("Cliente");
		lblCliente.setFont(new Font("Tahoma", Font.PLAIN, 14));

		comboBoxBetsClients = new JComboBox();
		comboBoxBetsClients.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JLabel lblValueBet = new JLabel("Valor apuesta");
		lblValueBet.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txtValueBet = new JTextField("1");
		txtValueBet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtValueBet.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				// Verificar si la tecla pulsada no es un digito
				if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' /* corresponde a BACK_SPACE */)) {
					e.consume(); // ignorar el evento de teclado
				}
			}
		});
		txtValueBet.setColumns(10);

		TabBetTypes = new JTabbedPane(JTabbedPane.TOP);

		btnBets = new JButton("Generar apuesta");
		btnBets.addActionListener(this);
		btnBets.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lblResultBets = new JLabel("");
		lblResultBets.setFont(new Font("Tahoma", Font.PLAIN, 14));
		loadTabBets();

		GroupLayout gl_panelBets = new GroupLayout(panelBets);
		gl_panelBets.setHorizontalGroup(gl_panelBets.createParallelGroup(Alignment.LEADING).addGroup(gl_panelBets
				.createSequentialGroup().addContainerGap()
				.addComponent(lblRoulette, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(18)
				.addGroup(gl_panelBets.createParallelGroup(Alignment.TRAILING).addGroup(gl_panelBets
						.createSequentialGroup()
						.addComponent(comboBoxBetsRoulette, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
						.addGap(26).addComponent(lblCliente, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(comboBoxBetsClients, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
						.addGap(28).addComponent(lblValueBet).addGap(18)
						.addComponent(txtValueBet, GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))
						.addGroup(gl_panelBets.createSequentialGroup()
								.addComponent(lblResultBets, GroupLayout.PREFERRED_SIZE, 382,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18).addComponent(btnBets))
						.addComponent(TabBetTypes, GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE))
				.addGap(67)));
		gl_panelBets
				.setVerticalGroup(gl_panelBets.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelBets.createSequentialGroup().addContainerGap()
								.addGroup(gl_panelBets.createParallelGroup(Alignment.BASELINE).addComponent(lblRoulette)
										.addComponent(comboBoxBetsRoulette, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblCliente)
										.addComponent(comboBoxBetsClients, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtValueBet, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblValueBet))
								.addGap(18)
								.addComponent(TabBetTypes, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_panelBets.createParallelGroup(Alignment.LEADING)
										.addComponent(lblResultBets, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
										.addComponent(btnBets))
								.addGap(23)));

		TabBetColor = new JLayeredPane();
		TabBetTypes.addTab("Realizar apuesta por color", null, TabBetColor, null);

		JPanel panelColor = new JPanel();
		GroupLayout gl_TabBetColor = new GroupLayout(TabBetColor);
		gl_TabBetColor.setHorizontalGroup(gl_TabBetColor.createParallelGroup(Alignment.TRAILING)
				.addComponent(panelColor, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE));
		gl_TabBetColor.setVerticalGroup(
				gl_TabBetColor.createParallelGroup(Alignment.LEADING).addGroup(gl_TabBetColor.createSequentialGroup()
						.addComponent(panelColor, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE).addGap(0)));

		JLabel lblColor = new JLabel("Seleccione el color de su apuesta");
		lblColor.setFont(new Font("Tahoma", Font.PLAIN, 14));

		comboBoxColor = new JComboBox();
		comboBoxColor.setModel(new DefaultComboBoxModel(new String[] { "Rojo", "Negro" }));
		GroupLayout gl_panelColor = new GroupLayout(panelColor);
		gl_panelColor.setHorizontalGroup(gl_panelColor.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelColor.createSequentialGroup().addContainerGap().addComponent(lblColor).addGap(18)
						.addComponent(comboBoxColor, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(163, Short.MAX_VALUE)));
		gl_panelColor.setVerticalGroup(gl_panelColor.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelColor.createSequentialGroup().addContainerGap()
						.addGroup(gl_panelColor.createParallelGroup(Alignment.BASELINE).addComponent(lblColor)
								.addComponent(comboBoxColor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addContainerGap(59, Short.MAX_VALUE)));
		panelColor.setLayout(gl_panelColor);
		TabBetColor.setLayout(gl_TabBetColor);

		TabBetNumber = new JLayeredPane();
		TabBetTypes.addTab("Realizar apuesta por numero", null, TabBetNumber, null);
		JPanel panelNumber = new JPanel();

		GroupLayout gl_TabBetNumber = new GroupLayout(TabBetNumber);
		gl_TabBetNumber.setHorizontalGroup(gl_TabBetNumber.createParallelGroup(Alignment.LEADING)
				.addComponent(panelNumber, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE));
		gl_TabBetNumber.setVerticalGroup(gl_TabBetNumber.createParallelGroup(Alignment.LEADING)
				.addComponent(panelNumber, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE));

		JLabel lblNumber = new JLabel("Seleccione el numero con el que desea realizar la apuesta");
		lblNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));

		comboBoxNumber = new JComboBox();
		comboBoxNumber.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8",
				"9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25",
				"26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36" }));
		GroupLayout gl_panelNumber = new GroupLayout(panelNumber);
		gl_panelNumber.setHorizontalGroup(gl_panelNumber.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNumber.createSequentialGroup().addContainerGap().addComponent(lblNumber).addGap(18)
						.addComponent(comboBoxNumber, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(100, Short.MAX_VALUE)));
		gl_panelNumber.setVerticalGroup(gl_panelNumber.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNumber.createSequentialGroup().addContainerGap()
						.addGroup(gl_panelNumber.createParallelGroup(Alignment.BASELINE).addComponent(lblNumber)
								.addComponent(comboBoxNumber, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addContainerGap(53, Short.MAX_VALUE)));
		panelNumber.setLayout(gl_panelNumber);
		TabBetNumber.setLayout(gl_TabBetNumber);
		panelBets.setLayout(gl_panelBets);
		tabBets.setLayout(gl_tabBets);

		tabCloseBet = new JLayeredPane();
		tabbedPane.addTab("Cerrar apuestas", null, tabCloseBet, null);

		panelCloseBets = new JPanel();

		lblCloseBets = new JLabel("Seleccione la ruleta que desea cerrar");
		lblCloseBets.setFont(new Font("Tahoma", Font.PLAIN, 14));

		comboBoxCloseBets = new JComboBox();
		comboBoxCloseBets.setFont(new Font("Tahoma", Font.PLAIN, 14));

		btnCloseBets = new JButton("Cerrar");
		btnCloseBets.addActionListener(this);
		btnCloseBets.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JScrollPane scrollPaneCloseBets = new JScrollPane();

		GroupLayout gl_panelCloseBets = new GroupLayout(panelCloseBets);
		gl_panelCloseBets.setHorizontalGroup(gl_panelCloseBets.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCloseBets.createSequentialGroup().addGroup(gl_panelCloseBets
						.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCloseBets.createSequentialGroup().addGap(10)
								.addComponent(lblCloseBets, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(comboBoxCloseBets, GroupLayout.PREFERRED_SIZE, 117,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18).addComponent(btnCloseBets))
						.addGroup(gl_panelCloseBets.createSequentialGroup().addContainerGap()
								.addComponent(scrollPaneCloseBets, GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)))
						.addGap(197)));
		gl_panelCloseBets.setVerticalGroup(gl_panelCloseBets.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCloseBets.createSequentialGroup().addGap(11).addGroup(gl_panelCloseBets
						.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCloseBets, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panelCloseBets.createSequentialGroup().addGap(1).addComponent(comboBoxCloseBets,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnCloseBets)).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(scrollPaneCloseBets, GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
						.addContainerGap()));

		tableCloseBets = new JTable();
		scrollPaneCloseBets.setViewportView(tableCloseBets);
		tableCloseBets.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		tableCloseBets.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tableCloseBets.setSurrendersFocusOnKeystroke(true);
		tableCloseBets.setCellSelectionEnabled(true);
		tableCloseBets.setColumnSelectionAllowed(true);
		tableCloseBets.setFillsViewportHeight(true);
		panelCloseBets.setLayout(gl_panelCloseBets);
		GroupLayout gl_tabCloseBet = new GroupLayout(tabCloseBet);
		gl_tabCloseBet.setHorizontalGroup(gl_tabCloseBet.createParallelGroup(Alignment.LEADING)
				.addComponent(panelCloseBets, GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE));
		gl_tabCloseBet.setVerticalGroup(gl_tabCloseBet.createParallelGroup(Alignment.LEADING)
				.addComponent(panelCloseBets, GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE));
		tabCloseBet.setLayout(gl_tabCloseBet);

		JLayeredPane tabAllRoulettes = new JLayeredPane();
		tabbedPane.addTab("Listado de ruletas", null, tabAllRoulettes, null);

		JPanel panelAllRoulettes = new JPanel();
		GroupLayout gl_tabAllRoulettes = new GroupLayout(tabAllRoulettes);
		gl_tabAllRoulettes.setHorizontalGroup(gl_tabAllRoulettes.createParallelGroup(Alignment.LEADING)
				.addComponent(panelAllRoulettes, GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE));
		gl_tabAllRoulettes.setVerticalGroup(gl_tabAllRoulettes.createParallelGroup(Alignment.LEADING)
				.addComponent(panelAllRoulettes, GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE));

		JScrollPane scrollPaneAllRoulette = new JScrollPane();
		GroupLayout gl_panelAllRoulettes = new GroupLayout(panelAllRoulettes);
		gl_panelAllRoulettes.setHorizontalGroup(gl_panelAllRoulettes.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelAllRoulettes.createSequentialGroup().addContainerGap()
						.addComponent(scrollPaneAllRoulette, GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
						.addGap(13)));
		gl_panelAllRoulettes.setVerticalGroup(gl_panelAllRoulettes.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelAllRoulettes.createSequentialGroup().addContainerGap()
						.addComponent(scrollPaneAllRoulette, GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
						.addContainerGap()));

		tableAllRoulettes = new JTable();
		tableAllRoulettes.setFillsViewportHeight(true);
		tableAllRoulettes.setColumnSelectionAllowed(true);
		tableAllRoulettes.setCellSelectionEnabled(true);
		tableAllRoulettes.setSurrendersFocusOnKeystroke(true);

		scrollPaneAllRoulette.setViewportView(tableAllRoulettes);
		panelAllRoulettes.setLayout(gl_panelAllRoulettes);
		tabAllRoulettes.setLayout(gl_tabAllRoulettes);
		contentPane.setLayout(gl_contentPane);
		loadTabCloseBets();
		loadTabAllRoulettes();
	}

	public void loadTabCreateRoulette() {
		lblResultCreateRoulette.setText("");
	}

	public void loadTabOpenRoulette() {
		lblResultOpenRoulette.setText("");
		comboBoxOpenRoulette.setModel(new DefaultComboBoxModel(casino.listCreatedIdRoulettes()));
		if (casino.listCreatedIdRoulettes().length > 0) {
			btnOpenRoulette.setVisible(true);
		} else {
			btnOpenRoulette.setVisible(false);
		}
	}

	public void loadTabBets() {
		lblResultBets.setText("");
		comboBoxBetsRoulette.setModel(new DefaultComboBoxModel(casino.listOpenIdRoulettes()));
		comboBoxBetsClients.setModel(new DefaultComboBoxModel(casino.listClients()));
		if (casino.listOpenIdRoulettes().length > 0) {
			btnBets.setVisible(true);
		} else {
			btnBets.setVisible(false);
		}
	}

	public void loadTabCloseBets() {
		comboBoxCloseBets.setModel(new DefaultComboBoxModel(casino.listOpenIdRoulettes()));
		tableCloseBets.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, },
				new String[] { "id cliente", "id apuesta", "Tipo de apuestas", "Valor apuestas" }));
		if (casino.listOpenIdRoulettes().length > 0) {
			btnCloseBets.setVisible(true);
		} else {
			btnCloseBets.setVisible(false);
		}
	}

	public void loadTabAllRoulettes() {
		String[][] allRoulettes = casino.allRoulette();
		tableAllRoulettes.setModel(new DefaultTableModel(allRoulettes, new String[] { "Ruleta", "Estado" }));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnCreateRoulette) {
			actionListenerCreateRoulette ();
		}
		if (e.getSource() == btnOpenRoulette) {
			actionListenerOpenRoulette ();
		}
		if (e.getSource() == btnBets) {
			actionListenerBets ();
		}
		if (e.getSource() == btnCloseBets) {
			actionListenerCloseBets ();
		}
		loadTabAllRoulettes();
	}
	
	public void actionListenerCreateRoulette (){
		int id = casino.createRoulette();
		lblResultCreateRoulette.setText("Se creo con exito la ruleta con id = " + id);
		loadTabOpenRoulette();
		loadTabBets();
		loadTabCloseBets();
	}
	
	public void actionListenerOpenRoulette () {
		String[] idsRoulette = casino.listCreatedIdRoulettes();
		int pos = comboBoxOpenRoulette.getSelectedIndex();
		String roulette = idsRoulette[pos];
		String positionId = roulette.substring(roulette.length() - 1);
		int idRoulette = casino.getRoulettes().get(Integer.parseInt(positionId)).getId();
		boolean result = casino.openRoulette(idRoulette);
		if (result == true) {
			loadTabOpenRoulette();
			lblResultOpenRoulette.setText("La ruleta con id #" + idRoulette + " se abrio exitosamente");
		} else {
			lblResultOpenRoulette.setText("Ocurrio un error");
		}
		loadTabCreateRoulette();
		loadTabBets();
		loadTabCloseBets();
	}
	
	public void actionListenerBets () {
		String[] idsRoulette = casino.listOpenIdRoulettes();
		int posRoulette = comboBoxBetsRoulette.getSelectedIndex();
		String roulette = idsRoulette[posRoulette];
		String positionId = roulette.substring(roulette.length() - 1);
		int idRoulette = casino.getRoulettes().get(Integer.parseInt(positionId)).getId();
		String[] clients = casino.listClients();
		int posClient = comboBoxBetsClients.getSelectedIndex();
		int userId = Integer.parseInt(clients[posClient]);
		String color = comboBoxColor.getSelectedItem().toString();
		int number = Integer.parseInt(comboBoxNumber.getSelectedItem().toString());
		int cost = Integer.parseInt(txtValueBet.getText());
		boolean result = false;
		if (cost <= 10000 && cost > 0) {
			if (!(TabBetNumber.isVisible() == true)) {
				result = casino.openBets(idRoulette, userId, -1, color, cost);
			} else if (!(TabBetColor.isVisible()) == true) {
				result = casino.openBets(idRoulette, userId, number, "NAN", cost);
			} else {
				lblResultBets.setText("Ocurrio un error al crear la apuesta");
			}
		} else {
			lblResultBets.setText("el valor de la apuesta debe de estar entre $1 a $10000");
		}
		if (result == true) {
			loadTabBets();
			lblResultBets.setText("Apuesta realizada exitosamente");
		}
		loadTabCreateRoulette();
		loadTabOpenRoulette();
		loadTabCloseBets();
	}
	
	public void actionListenerCloseBets () {
		String[] idsRoulette = casino.listOpenIdRoulettes();
		int pos = comboBoxCloseBets.getSelectedIndex();
		String roulette = idsRoulette[pos];
		String positionId = roulette.substring(roulette.length() - 1);
		int idRoulette = casino.getRoulettes().get(Integer.parseInt(positionId)).getId();
		String[][] bets = casino.closeBets(idRoulette);
		loadTabCloseBets();
		tableCloseBets.setModel(new DefaultTableModel(bets,
				new String[] { "id cliente", "id apuesta", "Tipo de apuestas", "Valor apuestas" }));
		loadTabCreateRoulette();
		loadTabOpenRoulette();
		loadTabBets();
	}
}
