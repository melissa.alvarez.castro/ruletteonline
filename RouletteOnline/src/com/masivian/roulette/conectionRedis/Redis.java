package com.masivian.roulette.conectionRedis;
import redis.clients.jedis.Jedis;

public class Redis {
	public Redis() {
		// Connecting to Redis server on localhost
		Jedis jedis = new Jedis("localhost");
		// check whether server is running or not
		System.out.println("ping!: " + jedis.ping()); 
	}
}
