package com.masivian.roulette.util;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.List;

import com.masivian.roulette.classes.Bet;
import com.masivian.roulette.classes.Casino;
import com.masivian.roulette.classes.Client;
import com.masivian.roulette.classes.Roulette;

/**
 * The Util class collects all the necessary methods for the operation of the
 * API.
 * 
 * @author melissa alvarez castro
 *
 */

public class Util {
	public static int findPosRoulette(List<Roulette> roulettes, int idRoulette) {
		int i = 0;
		for (Roulette roulette : roulettes) {
			if (roulette.getId() == idRoulette) {
				return i;
			}
			i++;
		}
		return -1;
	}

	public static String[] listCreatedIdRoulettes(List<Roulette> roulettes) {
		List<String> ids = new ArrayList<String>();
		for (Roulette roulette : roulettes) {
			boolean state = roulette.isState();
			//only roulettes with false status (close)
			if (state == false) {
				ids.add("Ruleta id = " + String.valueOf(roulette.getId()));
			}
		}
		return converterArrayLisToArrayStrings(ids);
	}

	public static String[] listOpenIdRoulettes(List<Roulette> roulettes) {
		List<String> ids = new ArrayList<String>();
		for (Roulette roulette : roulettes) {
			boolean state = roulette.isState();
			//only roulettes with true status (open)
			if (state == true) {
				ids.add("Ruleta id = " + String.valueOf(roulette.getId()));
			}
		}
		return converterArrayLisToArrayStrings(ids);
	}

	public static String[] converterArrayLisToArrayStrings(List<String> list) {
		String[] result = new String[list.size()];
		int i = 0;
		for (String element : list) {
			result[i] = element;
			i++;
		}
		return result;
	}

	public static String[] listClients(List<Client> clients) {
		List<String> ids = new ArrayList<String>();
		for (Client client : clients) {
			ids.add("" + client.getId());
		}
		return converterArrayLisToArrayStrings(ids);
	}

	public static String[][] createViewBets(List<Bet> bets) {
		// columns order : "id cliente", "id apuesta", "Tipo de apuestas", "Valor
		// apuestas"
		String[][] table = new String[bets.size()][4];
		for (int i = 0; i < table.length; i++) {
			table[i][0] = String.valueOf(bets.get(i).getUserId());
			table[i][1] = String.valueOf(bets.get(i).getId());
			table[i][2] = determineBetType(bets.get(i));
			table[i][3] = String.valueOf(bets.get(i).getCost());
		}
		return table;
	}

	public static String determineBetType(Bet bet) {
		String result = "";
		if (bet.getNumber() == -1 && !bet.getColor().contentEquals("NAN")) {
			result = "la apuesta se realiza con el color " + bet.getColor();
		} else if (bet.getNumber() != -1 && bet.getColor().contentEquals("NAN")) {
			result = "la apuesta se realiza con el numero " + bet.getNumber();
		} else {
			result = "Invalue";
		}
		return result;
	}

	public static String[][] allRoulette(List<Roulette> roulettes) {
		// columns order : "Ruleta", "Estado"
		String[][] table = new String[roulettes.size()][2];
		for (int i = 0; i < table.length; i++) {
			table[i][0] = "Ruleta id = " + String.valueOf(roulettes.get(i).getId());
			table[i][1] = "el estado ruleta = " + stateOpenClose(roulettes.get(i).isState());
		}
		return table;
	}

	public static String stateOpenClose(boolean state) {
		if (state == true) {
			return "abierto";
		} else {
			return "cerrado";
		}
	}

	public static void createClients(List<Client> clients) {
		Client client_1 = new Client(18273, "Melissa");
		Client client_2 = new Client(18863, "Camilo");
		Client client_3 = new Client(75373, "Juliana");
		clients.add(client_1);
		clients.add(client_2);
		clients.add(client_3);
	}
}
