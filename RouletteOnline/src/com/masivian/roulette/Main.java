package com.masivian.roulette;

import com.masivian.roulette.view.ViewMain;

public class Main {

	public static void main(String[] args) {
		//load the view and display it
		ViewMain viewMain = new ViewMain();
		viewMain.setVisible(true);
	}

}
