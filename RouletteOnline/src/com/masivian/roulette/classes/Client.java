package com.masivian.roulette.classes;

/**
 * The Client class represents the user.
 * 
 * @author melissa alvarez castro
 *
 */
public class Client {
	private int id;
	private String name;

	public Client(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	/**
	 * Getters and Setters
	 */
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
