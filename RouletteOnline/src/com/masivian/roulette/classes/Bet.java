package com.masivian.roulette.classes;

/**
 * The Bet class represents a real life a bet on a roulette.
 * 
 * @author melissa alvarez castro
 *
 */
public class Bet {
	private int rouletteId;
	private int id;
	private int userId;
	private int number;
	private String color;
	private int cost;

	public Bet(int rouletteId, int id, int userId, int number, int cost) {
		super();
		this.rouletteId = rouletteId;
		this.id = id;
		this.userId = userId;
		this.number = number;
		this.color = "NAN";
		this.cost = cost;
	}

	public Bet(int rouletteId, int id, int userId, String color, int cost) {
		super();
		this.rouletteId = rouletteId;
		this.id = id;
		this.userId = userId;
		this.number = -1;
		this.color = color;
		this.cost = cost;
	}
	
	/**
	 * Getters and Setters
	 */
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getRouletteId() {
		return rouletteId;
	}

	public void setRouletteId(int rouletteId) {
		this.rouletteId = rouletteId;
	}

}
