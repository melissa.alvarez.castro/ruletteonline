package com.masivian.roulette.classes;

import java.util.ArrayList;
import java.util.List;

import com.masivian.roulette.util.Util;

/**
 * The Casino class represents the environment of online betting roulette.
 * 
 * @author melissa alvarez castro
 *
 */
public class Casino {
	public List<Roulette> roulettes;
	public List<Client> clients;

	public Casino() {
		// init Lists Roulettes and Clients
		this.roulettes = new ArrayList<Roulette>();
		this.clients = new ArrayList<Client>();
		Util.createClients(clients);
	}

	public int createRoulette() {
		// create and add to list
		Roulette roulette = new Roulette(roulettes.size());
		roulettes.add(roulette);
		int id = roulette.getId();
		return id;
	}

	public boolean openRoulette(int idRoulette) {
		boolean result = false;
		// find the position of roulette in the list
		int posRoulette = Util.findPosRoulette(roulettes, idRoulette);
		if (posRoulette != -1) {
			// the true state in roulette means open
			roulettes.get(posRoulette).setState(true);
			result = true;
		}
		return result;
	}

	public boolean openBets(int idRoulette, int userId, int number, String color, int cost) {
		boolean result = false;
		// find the position of roulette in the list
		int posRoulette = Util.findPosRoulette(roulettes, idRoulette);
		if (posRoulette != -1) {
			// there are two types of bet by color or by number
			if (number == -1 && (color.equals("Rojo") || color.equals("Negro"))) {
				Bet bet = new Bet(idRoulette, roulettes.get(posRoulette).getBets().size(), userId, color, cost);
				roulettes.get(posRoulette).getBets().add(bet);
				result = true;
			} else {
				Bet bet = new Bet(idRoulette, roulettes.get(posRoulette).getBets().size(), userId, number, cost);
				roulettes.get(posRoulette).getBets().add(bet);
				result = true;
			}
		}
		return result;
	}

	public String[][] closeBets(int idRoulette) {
		List<Bet> bets = new ArrayList<Bet>();
		// find the position of roulette in the list
		int posRoulette = Util.findPosRoulette(roulettes, idRoulette);
		if (posRoulette != -1) {
			// the true state in roulette means close
			roulettes.get(posRoulette).setState(false);
			bets = roulettes.get(posRoulette).getBets();
		}
		// returns the attributes to display
		String[][] betsView = Util.createViewBets(bets);
		return betsView;
	}

	public List<Roulette> getRoulettes() {
		return roulettes;
	}

	public void setRoulettes(List<Roulette> roulettes) {
		this.roulettes = roulettes;
	}

	public String[] listCreatedIdRoulettes() {
		return Util.listCreatedIdRoulettes(roulettes);
	}

	public String[] listOpenIdRoulettes() {
		return Util.listOpenIdRoulettes(roulettes);
	}

	public String[] listClients() {
		return Util.listClients(clients);
	}

	public String[][] allRoulette() {
		return Util.allRoulette(roulettes);
	}
}
