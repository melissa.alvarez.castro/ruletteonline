package com.masivian.roulette.classes;

import java.util.ArrayList;
import java.util.List;

import com.masivian.roulette.util.Util;

/**
 * The Roulette class represents a real life Roulette where bets can be made.
 * 
 * @author melissa alvarez castro
 *
 */
public class Roulette {
	
	private int id;
	private boolean state;
	private List<Bet> bets;

	public Roulette(int id) {
		this.id = id;
		this.bets = new ArrayList<Bet>();
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public List<Bet> getBets() {
		return bets;
	}

	public void setBets(List<Bet> bets) {
		this.bets = bets;
	}
		
}
